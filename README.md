# Python API with database


## Dependencies
This is a CRUD Restful API with python and Flask, that also is connected into 
a sqlite database. In order to run this project you need to:

1. Clone the repo
2. Run in the terminal ``pip install Flask`` in order to have the right dependencies
3. Run the following commands:
```bash 
$ pip install flask_sqlalchemy
$ pip install flask_marshmallow
$ pip install marshmallow-sqlalchemy
```
4. Enter to the interactive shell using writing in the terminal ``python``
5. Import the db object ussing: 
```bash 
>>> from crud import db
>>> db.create_all()
```
6. Now that you generate the sqlite db you can run the application in the terminal using:
````bash
python crud.py
````

*Note: this project has 2 files, one is a basic endpoint called app.py and a complex one called crud.py

## Endpoints

In this project you will find 5 endpoints, that you can check in the [yaml file](swagger.yaml)

- Create User (POST)
- Get all users (GET)
- Get user by id (GET)
- Update user by id (PUT)
- Delete user by id (DELETE)

I hope you enjoy this app created and inspired by the following [Medium Article](https://medium.com/python-pandemonium/build-simple-restful-api-with-python-and-flask-part-2-724ebf04d12), 
some parts have changed in order to be more personal to me.